package com.epam.currency.exchange.service.impl;

import com.epam.currency.exchange.model.Rate;
import com.epam.currency.exchange.model.exception.RateNotFoundException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StubCurrencyServiceImplTest {

    private static final double USD_TO_RUB_RATE = 64.52;

    private final StubCurrencyServiceImpl service = new StubCurrencyServiceImpl();

    @Test
    public void testGetRate() throws Exception{
        String fromCurrency = "USD";
        String toCurrency = "RUB";

        Rate rate = service.getRate(fromCurrency, toCurrency);

        assertNotNull(rate);
        assertEquals(fromCurrency, rate.getFrom().name());
        assertEquals(toCurrency, rate.getTo().name());
        assertEquals(USD_TO_RUB_RATE, rate.getExchangeRate(), 0);
    }

    @Test(expected = RateNotFoundException.class)
    public void testGetRateForNotExistedCurrencies() throws Exception{
        String fromCurrency = "AUSD";
        String toCurrency = "AEUR";

        service.getRate(fromCurrency, toCurrency);
    }

    @Test
    public void testExchange() throws Exception{
        String fromCurrency = "USD";
        String toCurrency = "RUB";
        double fromAmount = 1.0;

        double toAmount = service.exchange(fromCurrency, toCurrency, fromAmount);

        assertEquals(USD_TO_RUB_RATE * fromAmount, toAmount,0);
    }

    @Test(expected = RateNotFoundException.class)
    public void testExchangeNotExistedCurrencies() throws Exception{
        String fromCurrency = "AUSD";
        String toCurrency = "AEUR";
        double fromAmount = 1.0;

        service.exchange(fromCurrency, toCurrency, fromAmount);
    }
}
