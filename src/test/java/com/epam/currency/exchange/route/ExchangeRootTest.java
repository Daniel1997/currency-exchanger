package com.epam.currency.exchange.route;

import com.epam.currency.exchange.generated.ExchangeRequest;
import com.epam.currency.exchange.generated.ExchangeRequestList;
import com.epam.currency.exchange.model.ExchangeResponse;
import org.apache.camel.Exchange;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringTestSupport;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.util.Arrays;

public class ExchangeRootTest extends CamelSpringTestSupport {

    private static final String CONTEXT_LOCATION = "camelTestContext.xml";
    private static final String REPLACED_FROM_ENDPOINT = "direct:start";
    private static final String MOCKED_TO_ENDPOINT = "mock:end";
    private static final String JMS_QUEUE_ENDPOINT = "activemq:queue:CurrencyExchangeQueue";

    private static final double USD_TO_RUB_RATE = 64.52;
    private static final double EUR_TO_RUB_RATE = 72.84;

    @Override
    protected AbstractApplicationContext createApplicationContext() {
        return new ClassPathXmlApplicationContext(CONTEXT_LOCATION);
    }

    @Before
    public void mockEndpoints() throws Exception {
        AdviceWithRouteBuilder adviceWithRouteBuilder = new AdviceWithRouteBuilder() {
            @Override
            public void configure() throws Exception {
                replaceFromWith(REPLACED_FROM_ENDPOINT);
                interceptSendToEndpoint(JMS_QUEUE_ENDPOINT)
                        .skipSendToOriginalEndpoint()
                        .to(MOCKED_TO_ENDPOINT);
            }
        };
        context.getRouteDefinitions().get(0).adviceWith(context, adviceWithRouteBuilder);
    }

    @Test
    public void testExchange() throws Exception {
        ExchangeRequest request0 = createRequest("USD", 1.0, "RUB");
        ExchangeRequest request1 = createRequest("EUR", 2.0, "RUB");
        ExchangeRequestList requestList = createRequestList(request0, request1);
        MockEndpoint end = getMockEndpoint(MOCKED_TO_ENDPOINT);

        template.sendBody(REPLACED_FROM_ENDPOINT, requestList);

        end.expectedMessageCount(2);
        ExchangeResponse response0 = convertToResponse(end.getReceivedExchanges().get(0));
        ExchangeResponse response1 = convertToResponse(end.getReceivedExchanges().get(1));
        assertResponse(response0, request0.getFrom(), request0.getAmount(), request0.getTo(), USD_TO_RUB_RATE * request0.getAmount());
        assertResponse(response1, request1.getFrom(), request1.getAmount(), request1.getTo(), EUR_TO_RUB_RATE * request1.getAmount());
    }

    private ExchangeRequestList createRequestList(ExchangeRequest... requests) throws NoSuchFieldException, IllegalAccessException {
        ExchangeRequestList exchangeRequestList = new ExchangeRequestList();
        Field field = ExchangeRequestList.class.getDeclaredField("exchangeRequest");
        field.setAccessible(true);
        field.set(exchangeRequestList, Arrays.asList(requests));
        return exchangeRequestList;
    }

    private ExchangeRequest createRequest(String fromCurrency, double amount, String toCurrency) {
        ExchangeRequest request = new ExchangeRequest();
        request.setFrom(fromCurrency);
        request.setAmount(amount);
        request.setTo(toCurrency);
        return request;
    }

    private ExchangeResponse convertToResponse(Exchange exchange) throws JAXBException {
        byte[] jaxbResponse = (byte[]) exchange.getIn().getBody();
        JAXBContext jaxbContext = JAXBContext.newInstance(ExchangeResponse.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (ExchangeResponse) unmarshaller.unmarshal(new ByteArrayInputStream(jaxbResponse));
    }

    private void assertResponse(ExchangeResponse response, String expFromCurrency, double expFromAmount,
                                String expToCurrency, double expToAmount) {
        assertEquals(expFromCurrency, response.getFrom());
        assertEquals(expFromAmount, response.getFromAmount(), 0);
        assertEquals(expToCurrency, response.getTo());
        assertEquals(expToAmount, response.getToAmount(), 0);
    }
}
