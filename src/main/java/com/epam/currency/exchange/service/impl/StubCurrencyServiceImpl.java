package com.epam.currency.exchange.service.impl;

import com.epam.currency.exchange.model.Rate;
import com.epam.currency.exchange.model.exception.RateNotFoundException;
import com.epam.currency.exchange.service.CurrencyService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.epam.currency.exchange.model.Currency.EUR;
import static com.epam.currency.exchange.model.Currency.RUB;
import static com.epam.currency.exchange.model.Currency.USD;

public class StubCurrencyServiceImpl implements CurrencyService {

    private final List<Rate> rates;

    public StubCurrencyServiceImpl() {
        rates = new ArrayList<Rate>(
                Arrays.asList(
                        new Rate(RUB, USD, 0.015),
                        new Rate(RUB, EUR, 0.014),
                        new Rate(USD, RUB, 64.52),
                        new Rate(USD, EUR, 0.89),
                        new Rate(EUR, USD, 1.13),
                        new Rate(EUR, RUB, 72.84)
                ));
    }

    public Rate getRate(String fromCurrency, String toCurrency) throws RateNotFoundException {
        for (Rate rate : rates) {
            if (rate.getFrom().name().equals(fromCurrency) && rate.getTo().name().equals(toCurrency)) {
                return rate;
            }
        }
        throw new RateNotFoundException();
    }

    public double exchange(String fromCurrency, String toCurrency, double amount) throws RateNotFoundException {
        return getRate(fromCurrency, toCurrency).getExchangeRate() * amount;
    }
}
