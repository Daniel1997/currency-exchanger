package com.epam.currency.exchange.service;

import com.epam.currency.exchange.model.Rate;
import com.epam.currency.exchange.model.exception.RateNotFoundException;

public interface CurrencyService {
    Rate getRate(String fromCurrency, String toCurrency) throws RateNotFoundException;

    double exchange(String fromCurrency, String toCurrency, double amount) throws RateNotFoundException;
}
