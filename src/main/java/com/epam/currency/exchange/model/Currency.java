package com.epam.currency.exchange.model;

public enum Currency {
    USD, EUR, RUB
}
