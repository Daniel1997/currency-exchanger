package com.epam.currency.exchange.model;

public class Rate {
    private final Currency from;
    private final Currency to;
    private final double exchangeRate;

    public Rate(Currency from, Currency to, double exchangeRate) {
        this.from = from;
        this.to = to;
        this.exchangeRate = exchangeRate;
    }

    public Currency getFrom() {
        return from;
    }

    public Currency getTo() {
        return to;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }
}
