package com.epam.currency.exchange.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "exchangeResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExchangeResponse {
    @XmlElement
    private String from;
    @XmlElement
    private double fromAmount;
    @XmlElement
    private String to;
    @XmlElement
    private double toAmount;

    public String getFrom() {
        return from;
    }

    public double getFromAmount() {
        return fromAmount;
    }

    public String getTo() {
        return to;
    }

    public double getToAmount() {
        return toAmount;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setFromAmount(double fromAmount) {
        this.fromAmount = fromAmount;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setToAmount(double toAmount) {
        this.toAmount = toAmount;
    }
}
