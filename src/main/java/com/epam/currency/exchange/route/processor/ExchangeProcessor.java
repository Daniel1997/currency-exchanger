package com.epam.currency.exchange.route.processor;

import com.epam.currency.exchange.generated.ExchangeRequest;
import com.epam.currency.exchange.model.ExchangeResponse;
import com.epam.currency.exchange.service.CurrencyService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ExchangeProcessor implements Processor {
    private CurrencyService currencyService;

    public ExchangeProcessor(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    public void process(Exchange exchange) throws Exception {
        ExchangeRequest request = exchange.getIn().getBody(ExchangeRequest.class);
        double toAmount = currencyService.exchange(request.getFrom(), request.getTo(), request.getAmount());
        ExchangeResponse response = new ExchangeResponse();
        response.setFrom(request.getFrom());
        response.setFromAmount(request.getAmount());
        response.setTo(request.getTo());
        response.setToAmount(toAmount);
        exchange.getOut().setBody(response);
    }
}
