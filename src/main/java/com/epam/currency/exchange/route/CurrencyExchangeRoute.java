package com.epam.currency.exchange.route;

import com.epam.currency.exchange.generated.ExchangeRequest;
import com.epam.currency.exchange.generated.ExchangeRequestList;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;

import java.util.List;

public class CurrencyExchangeRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("cxf:bean:cxfSoapServiceEndpoint")
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        List<ExchangeRequest> requests = exchange.getIn().getBody(ExchangeRequestList.class).getExchangeRequest();
                        exchange.getOut().setBody(requests);
                    }
                })
                .log("Receiving request ${body}")
                .split(body())
                .log("Exchange ${body}")
                .processRef("exchangeProcessor")
                .log("Currency was successfully exchanged: ${body}")
                .marshal().jaxb()
                .log("Response ${body} was converted to xml")
                .to(ExchangePattern.InOnly,"activemq:queue:CurrencyExchangeQueue");
    }
}
